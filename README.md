# symfony_csv



## Getting started

Pour faire tourner : 


```
git clone git@gitlab.com:keulu/symfony_csv.git
cd symfony_csv
composer install
```

mettre a jour les données de connexion bdd dans le fichier .env et jouer la migration

```
php bin/console doctrine:migrations:migrate
```

lancer le serveur


```
php -s 127.0.0.1:8080 -t public
```

Routes exposées : 


```
GET /api/file - liste des ligne du csv
GET /api/file/id - une ligne du CSV 
POST /api/file - envoi et traitement de fichier csv
PUT /api/file/id - modification ligne csv
DELETE /api/file/id - suppression ligne
```