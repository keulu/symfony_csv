<?php
 
namespace App\Controller;
 
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HtmlSanitizer\HtmlSanitizerInterface;

use App\Entity\File;
use App\Form\FileType;
use App\Service\FileUploader;
 
/**
 * @Route("/api", name="api_")
 */
 
class FileController extends AbstractController
{
    /**
    * @Route("/file", name="file_index", methods={"GET"})
    */
    public function index(ManagerRegistry $doctrine): Response
    {
        $files = $doctrine
            ->getRepository(File::class)
            ->findAll();
  
        $data = [];
  
        foreach ($files as $file) {
           $data[] = [
               'id' => $file->getId(),
               'band_name' => $file->getBandName(),
               'country' => $file->getCountry(),
               'city' => $file->getCity(),
               'year_start' => $file->getYearstart(),
               'year_stop' => $file->getYearStop(),
               'creators' => $file->getCreators(),
               'style' => $file->getStyle(),
               'nb_members' => $file->getNbMembers(),
               'description' => $file->getDescription(),
           ];
        }

        if (sizeof($data) == 0) {
            return $this->json('No files found', 404);
        }
  
  
        return $this->json($data);
    }
 
  
    /**
     * @Route("/file", name="file_new", methods={"POST"})
     */
    public function new(ManagerRegistry $doctrine, Request $request, FileUploader $fileUploader, SluggerInterface $slugger, HtmlSanitizerInterface $htmlSanitizer): Response
    {
        // envoi de CSV
        $file = new File();

        $uploadFile = $request->files->get('file');

        if ($uploadFile) {
            $originalFilename = pathinfo($uploadFile->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $slugger->slug($originalFilename);
            $newFilename = $safeFilename.'-'.uniqid().'.csv';

            // Move the file to the directory where brochures are stored
            try {
                $uploadFile->move(
                    $this->getParameter('files_directory'),
                    $newFilename
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }

            $file->setFileFilename($newFilename);

            if (($handle = fopen($this->getParameter('files_directory') . '/' . $newFilename, "r")) !== FALSE) {

                // on nettoie les entrées
                $inserted_rows = $doctrine
                    ->getRepository(File::class)
                    ->findAll();
        
                $data = [];

                $entityManager = $doctrine->getManager();
        
                foreach ($inserted_rows as $row) {
                    $entityManager->remove($row);
                    $entityManager->flush();
                }

                $row = 1;
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    if ($row  > 1) {
                        $file = new File();    
                        $file->setBandName($htmlSanitizer->sanitize($data[0]));
                        $file->setCountry($htmlSanitizer->sanitize($data[1]));
                        $file->setCity($htmlSanitizer->sanitize($data[2]));
                        $file->setYearStart((int) $htmlSanitizer->sanitize($data[3]));
                        $file->setYearStop((int) $htmlSanitizer->sanitize($data[4]));
                        $file->setCreators($htmlSanitizer->sanitize($data[5]));
                        $file->setNbMembers((int) $htmlSanitizer->sanitize($data[6]));
                        $file->setStyle($htmlSanitizer->sanitize($data[7]));
                        $file->setDescription($htmlSanitizer->sanitize(utf8_decode($data[8])));
                        
                        $entityManager->persist($file);
                        $entityManager->flush();
                    }
                    $row++;
                }
                fclose($handle);
            } else {
                return $this->json('cant open file', 422);
            }

        } else {
            return $this->json($request, 422);
        }

        return $this->json('Created new file successfully');
    }
  
    /**
     * @Route("/file/{id}", name="file_show", methods={"GET"})
     */
    public function show(ManagerRegistry $doctrine, int $id): Response
    {
        $file = $doctrine->getRepository(File::class)->find($id);
  
        if (!$file) {
  
            return $this->json('No file found for id' . $id, 404);
        }
  
        $data =  [
            'id' => $file->getId(),
            'band_name' => $file->getBandName(),
            'country' => $file->getCountry(),
            'city' => $file->getCity(),
            'year_start' => $file->getYearstart(),
            'year_stop' => $file->getYearStop(),
            'creators' => $file->getCreators(),
            'style' => $file->getStyle(),
            'nb_members' => $file->getNbMembers(),
            'description' => $file->getDescription(),
        ];
          
        return $this->json($data);
    }
  
    /**
     * @Route("/file/{id}", name="file_edit", methods={"PUT"})
     */
    public function edit(ManagerRegistry $doctrine, Request $request, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $file = $entityManager->getRepository(File::class)->find($id);
  
        if (!$file) {
            return $this->json('No file found for id' . $id, 404);
        }

        $file->setBandName($request->request->get('band_name'));
        $file->setCountry($request->request->get('country'));
        $file->setCity($request->request->get('city'));
        $file->setYearStart($request->request->get('year_start'));
        $file->setYearStop($request->request->get('year_stop'));
        $file->setCreators($request->request->get('creators'));
        $file->setStyle($request->request->get('style'));
        $file->setNbMembers($request->request->get('nb_members'));
        $file->setDescription($request->request->get('description'));
        $entityManager->flush();
  
        $data =  [
            'id' => $file->getId(),
            'band_name' => $file->getBandName(),
            'country' => $file->getCountry(),
            'city' => $file->getCity(),
            'year_start' => $file->getYearstart(),
            'year_stop' => $file->getYearStop(),
            'creators' => $file->getCreators(),
            'style' => $file->getStyle(),
            'nb_members' => $file->getNbMembers(),
            'description' => $file->getDescription(),
        ];
          
        return $this->json($data);
    }
  
    /**
     * @Route("/file/{id}", name="file_delete", methods={"DELETE"})
     */
    public function delete(ManagerRegistry $doctrine, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $file = $entityManager->getRepository(File::class)->find($id);
  
        if (!$file) {
            return $this->json('No file found for id' . $id, 404);
        }
  
        $entityManager->remove($file);
        $entityManager->flush();
  
        return $this->json('Deleted a file successfully with id ' . $id);
    }
}