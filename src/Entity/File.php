<?php

namespace App\Entity;

use App\Repository\FileRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FileRepository::class)]
class File
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $country = null;

    #[ORM\Column(length: 255)]
    private ?string $city = null;

    #[ORM\Column]
    private ?int $year_start = null;

    #[ORM\Column(nullable: true)]
    private ?int $year_stop = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $creators = null;

    #[ORM\Column(nullable: true)]
    private ?int $nb_members = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $style = null;

    #[ORM\Column(length: 255)]
    private ?string $band_name = null;

    private $file_filename;

    public function getFileFilename()
    {
        return $this->file_filename;
    }

    public function setFileFilename($file_filename)
    {
        $this->file_filename = $file_filename;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getYearStart(): ?int
    {
        return $this->year_start;
    }

    public function setYearStart(int $year_start): self
    {
        $this->year_start = $year_start;

        return $this;
    }

    public function getYearStop(): ?int
    {
        return $this->year_stop;
    }

    public function setYearStop(?int $year_stop): self
    {
        $this->year_stop = $year_stop;

        return $this;
    }

    public function getCreators(): ?string
    {
        return $this->creators;
    }

    public function setCreators(?string $creators): self
    {
        $this->creators = $creators;

        return $this;
    }

    public function getNbMembers(): ?int
    {
        return $this->nb_members;
    }

    public function setNbMembers(?int $nb_members): self
    {
        $this->nb_members = $nb_members;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStyle(): ?string
    {
        return $this->style;
    }

    public function setStyle(?string $style): self
    {
        $this->style = $style;

        return $this;
    }

    public function getBandName(): ?string
    {
        return $this->band_name;
    }

    public function setBandName(string $band_name): self
    {
        $this->band_name = $band_name;

        return $this;
    }
}
